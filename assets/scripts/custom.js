//Form parameters: classes, tooltips, validation
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                let width = window.innerWidth;
                if (form.checkValidity() === false) {
                    $('.form-control').each(function (index, element) {
                        //To display tooltips is form input is not valid
                        if ($(element).is(':valid')) {
                            $(element).addClass('form-control:valid');
                            $(element).addClass('was-validated');
                            $(element).tooltip('disable');
                        } else {
                            $(element).addClass('form-control:invalid');
                            $(element).addClass('was-validated');
                            //To display tooltips only on bigger screens
                            if (width >= 800) {
                                $(element).tooltip('enable');
                                $(element).tooltip('show');
                            } else {
                                $(element).tooltip('disable');
                            }
                        }
                    });
                    if ($('#invalidCheck').is(':checked')) {
                        $('.form-check-label').tooltip('disable');
                    } else {
                        //To display tooltips only on bigger screens
                        if (width >= 800) {
                            $('.form-check-label').tooltip('enable').tooltip('show');
                        } else {
                            $('.form-check-label').tooltip('disable');
                        }
                    }
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

//Slower navigation
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });

        //To remove overlay and navigation when clicked on burger
        if ($(".navbar-collapse").hasClass("show")) {
            $(".navbar-collapse").removeClass("show");
            $(".menu-overlay").removeClass("show");
        }
    })
});

//Overlay when burger navigation is clicked
$(".navbar-toggler").click(function () {
    console.log('click');
    $(".menu-overlay").toggleClass("show").css("display", "");
    /*$(".navbar-collapse").removeClass("show");
    $(".menu-overlay").removeClass("show");*/

});

//To remove overlay and navigation when clicked on overlay
$(".menu-overlay").on('click', function (e) {
    e.preventDefault();
    $(".navbar-collapse").removeClass("show");
    $(".menu-overlay").removeClass("show");
})
